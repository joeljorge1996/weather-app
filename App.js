                                                                   
import React from 'react';
import SearchInput from './components/SearchInput';
import { StyleSheet,
         View,
         Text,
         ImageBackground,
         KeyboardAvoidingView, 
         Platform, 
         
         } from 'react-native';

var bgImage = require('./assets/4.jpg');

 


 export default class App extends React.Component {

  //iniatialises state.
  constructor(props) {
    super(props);
    this.state = { 
      location: '',
    };
  }

  //this calls method as soon as component
  //has mounted.
  componentDidMount(){
    this.handleUpdateLocation('San Francisco')
  }


  //this method changes the name of the city.
  handleUpdateLocation = city => {
    this.setState({ 
      location: city,
    });
  };
  
render(){

  const { location } = this.state;


  return (
 
    
   <KeyboardAvoidingView style={styles.container} behavior="padding">
      <ImageBackground
        source={bgImage}
        style={styles.imageContainer}
        imageStyle={styles.image}
      >
         <View style={styles.detailsContainer}>
           <Text style={[styles.largeText, styles.textStyle]}>{ location }</Text>
           <Text style={[styles.smallText, styles.textStyle]}>Light Cloud</Text>
           <Text style={[styles.largeText, styles.textStyle]}>24°</Text>
        

           <SearchInput 
              placeholder="Search any city"
              onSubmit={this.handleUpdateLocation}
              />
        </View>
      </ImageBackground>
      
  </KeyboardAvoidingView>    

 
  );

}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#34495E',
     
  },
  
  imageContainer: {
    flex: 1,
},
  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover',
  },
  detailsContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.2)',
    paddingHorizontal: 20,
  }
  ,

  textStyle: {
    textAlign: 'center',
    fontFamily: Platform.OS === 'ios' ? 'AvenirNext-Regular' : 'Roboto',
    color: 'white',
  },

  largeText: {
    fontSize: 44,
  },
  smallText: {
    fontSize: 18,
  },

  

});

 